# Study Stuff for Ansible Exam DO407
This repo is used as a study guide for exam [DO407](https://www.redhat.com/en/services/training/ex407-red-hat-certified-specialist-in-ansible-automation-exam) for Ansible. The following topics need to be covered:

- Understand core components of Ansible
    - [Inventories](#inventory)
    - Modules
    - Variables
    - Facts
    - Plays
    - Playbooks
    - Configuration files

- Run ad-hoc Ansible commands
- Use both static and dynamic inventories to define groups of hosts
- Utilize an existing dynamic inventory script
- Create Ansible plays and playbooks
    - Know how to work with commonly used Ansible modules
    - Use variables to retrieve the results of running a commands
    - Use conditionals to control play execution
    - Configure error handling
    - Create playbooks to configure systems to a specified state
    - Selectively run specific tasks in playbooks using tags
- Create and use templates to create customized configuration files
- Work with Ansible variables and facts
- Create and work with roles
- Download roles from an Ansible Galaxy and use them
- Manage parallelism
- Use Ansible Vault in playbooks to protect sensitive data
- Install Ansible Tower and use it to manage systems
- Use provided documentation to look up specific information about Ansible modules and commands


Will provide notes as well as code samples to acheive the above. 

# Lab Setup
I am currently switching back and forth between my local Mac and some servers that I can create with my [LinuxAcademy](https://linuxacademy.com) account. 

After you create these servers (or for any machines you wish to manage) you will need to `ssh-copy-id` between all of your managed machines OR disable host key check from ansible (which I do not advise in this scenario). 

NOTE: That when running from Mac, I must specify `--user` that exists on the managed machine for these to work. I am just using the default users created on the L.A. nodes which is `user`. 

To install `ansible` locally on your Mac you can issue through brew.
```
brew install ansible
```

# Study Sources
- Linux Academy's [Red Hat Certified Specialist in Ansible Automation (EX407) Preparation Course](https://linuxacademy.com/linux/training/course/name/linux-academy-red-hat-certified-specialist-in-ansible-automation)

- [RH's Ansible DO407 Course](https://www.redhat.com/en/services/training/do407-automation-ansible-i)

- [Official Ansible Docs](https://docs.ansible.com)

- MORE TO COME

# Notes

### Installation
``` 
# On control Node ONLY
yum install ansible -y
```

### Inventory
Create a file and place a list of hosts. You will reference this as an argument/override (`-i`, `--inventory` or `--inventory-file`) or specify it in your `ansible.cfg`. You can group nodes together by placing `[ ]` around them. 

```
cat << EOF >> inventory
[control]
wbassler231.mylabserver.com
EOF

# Test the inventory
ansible control -i inventory --list-hosts 
  hosts (1):
    wbassler231.mylabserver.com
```

-There are static (text/.ini) and dynamic (scripted) types of inventory. 

-2 Groups exist no matter what: `all` and `ungrouped`

-Nested Groups: Multiple groups combined together with the `:children` suffix.
```
cat << EOF >> inventory
[usa]
wbassler232.mylabserver.com

[canada]
wbassler233.mylabserver.com

[north-america:children]
canada
usa
EOF

ansible -i inventory -m ping north-america
wbassler232.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
wbassler233.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

-You can specify ranges using [START:END]. Example: `wbassler[2:5].com` would include nodes wbassler2.com - wbassler5.com.




For official Docs for [Inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

